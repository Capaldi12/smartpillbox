package com.nikadmin.smartpillbox;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.YY kk:mm");

    private MQTTController mqtt;
    private DBController database;
    private Timer timer;

    //Represents message. Method can be added to get payload straight from enum member
    enum Message {
        TAKEPILL,
        PILLTAKEN,
        PILLNOTTAKEN,
        INCORRECT;

        //Get enum object from string representation, replacing not matched ones with INCORRECT
        public static Message decode(String text) {
            try {
                return Message.valueOf(text);
            } catch (IllegalArgumentException e) {
                return Message.INCORRECT;
            }
        }

        //For-screen string representation
        public String represent() {
            switch (this) {
                case TAKEPILL:
                    return "Send message";
                case PILLTAKEN:
                    return "Pill was taken";
                case PILLNOTTAKEN:
                    return "Pill was not taken";
                default:
                    return "Incorrect entry";
            }
        }
    }

    private int delay = 5 * 60 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Place to retrieve current time zone instead of hardcoded one
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+4"));

        database = new DBController(this);
        mqtt = MQTTController.getInstance();

        mqtt.init(new MQTTController.ControllerCallback() {
            @Override
            public void onMessage(String message) {

                database.log(message);
                displayLog();

                Message decoded = Message.decode(message);

                if (decoded == Message.PILLTAKEN && timer != null) {
                    timer.cancel();
                    timer = null;
                    setStatus(decoded.represent());
                }

            }
        }, this);


        Button sendButton = findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (timer == null) {    //null timer means timer is not running now

                    mqtt.sendMessage(Message.TAKEPILL);
                    setStatus("Message sent");

                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            //without runOnUiThread callback can not get access to UI elements
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    timer = null;   //set null to state that timer is not running
                                    mqtt.sendMessage(Message.PILLNOTTAKEN);
                                    setStatus("Pill was not taken!");
                                }
                            });
                        }
                    }, delay);
                }
            }
        });

        Button clearButton = findViewById(R.id.clearButton);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.clear();
                displayLog();
            }
        });

        displayLog();
    }

    private void displayLog() {
        List<DBController.LogLine> log = database.getLastLogs(20);  //20 last records fit perfectly
        TextView logText = findViewById(R.id.logText);
        StringBuilder logString = new StringBuilder();

        for (DBController.LogLine line : log) {
            String date = dateFormat.format(new Date(line.datetime * 1000));    // * 1000, because time stored in milliseconds
            String message = Message.decode(line.message).represent();
            logString.append(String.format("[%s]: %s\n", date, message));
        }

        logText.setText(logString.toString());
    }

    public void setStatus(String text) {
        TextView statusText = findViewById(R.id.statusText);
        statusText.setText(text);
    }
}
